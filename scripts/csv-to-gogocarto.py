import json

source_data = json.loads(open("professionnels_du_st_ck.umap", 'r').read())

pros = []
for layer in source_data["layers"]:
    category = layer["_umap_options"]["name"] 
    
    out = {}
    for item in layer["features"]:
        entry = item["properties"]

        out["categories"] = [category]
        out["name"] = entry["name"]
        out["fullAdress"] = entry["Adresse"]

        out["streetAdress"] = re.split(r' \d{5} ', entry["Adresse"])[0]
        out["locality"] = re.split(r' \d{5} ', entry["Adresse"])[1] 
        out["postalCode"] = re.search("\d{5}", entry["Adresse"])[0]

        out["country"] = "fr"
        
        try:
            out["email"] = entry["Email"]
        except:
            pass
        try:
            out["phone"] = entry["Téléphone"]
        except:
            pass
        try:
            out["website"] = entry["Site web"]
        except:
            pass

        try:
            out["comment"] = entry["Description"]
        except:
            pass

        out["geo"] = {}
        out["geo"]["latitude"] = item["geometry"]['coordinates'][1]
        out["geo"]["longitude"] = item["geometry"]['coordinates'][0]

        pros.append(out)

gogocarto = {
    "licence": "Not free licensed, sorry!",
    "ontology":"gogofull",
    "data": pros
        }
json.dump(gogocarto, open("annuaire/stuck.json", 'w'))

