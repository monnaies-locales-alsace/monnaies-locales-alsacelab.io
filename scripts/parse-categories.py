import json
data = json.loads(open("/tmp/mozilla_u0/professionnels_du_st_ck.umap", 'r').read())

results_layers = []

for layer in data["layers"]:
    category = layer["_umap_options"]["name"]

    features_interm = []
    for feature in layer["features"]:
        feature["categories"] = [category, "Boutiques"]
        features_interm.append(feature)

    results_layers.append(features_interm)


results = {"layers": results_layers}

json.dump(results, open("/tmp/pros-stuck.json", 'w'))
